import test from "ava";
import { Bytes } from '../lib/bytes.js'
import { PrivateKey } from "../lib/private-key.js";

test("signMessageElectron matches Electron Cash Signing", async t => {
	const privateKey = PrivateKey.fromWIF('KyerGhLWTejDqwKNQtZrzBf1xtRcdmMdiUwgy6DCWjzgWN4hSmDM');

	const signature = new Bytes(privateKey.signMessageElectron('TESTING_BITCOIN_SIGNED_MESSAGE'));

	t.is(signature.toBase64(), 'H6VOt5dz4Gmb+M+Nbv9ulvHqtWAFlz9PacZ+3Qo43WdlMvLhaxd0Bni5MyJNlAoOF+E2ncUMIJnJB4KkMRTZrbk=');
});
