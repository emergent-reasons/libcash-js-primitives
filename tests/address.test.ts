import test from "ava";
import { Address } from "../lib/address.js";
import { Bytes } from "../lib/bytes.js";

// P2PKH Address
const CASH_ADDR_P2PKH = 'bitcoincash:qpeqv9k9j3l7fepp9du24f9pr5jdu7c2mvn4wwldav';
const LEGACY_ADDRESS_P2PKH = '1BPuKwKMywTtfBmt8aTBFk8StQtHdnqdxQ'
const RIPEMD160_P2PKH_HEX = '720616c5947fe4e4212b78aaa4a11d24de7b0adb';
const RIPEMD160_P2PKH = Bytes.fromHex(RIPEMD160_P2PKH_HEX).toBytes();

// P2SH Address
const CASH_ADDR_P2SH = 'bitcoincash:pzxppqu9cs62y0q57gursuekhflrr2f4xsduayy82l';
const LEGACY_ADDRESS_P2SH = '3ETcJo8Yda46wg3rwGmcnUCRN81G2wEVua';
const RIPEMD160_P2SH_HEX = '8c108385c434a23c14f238387336ba7e31a93534';
const RIPEMD160_P2SH = Bytes.fromHex(RIPEMD160_P2SH_HEX).toBytes();
const LOCKSCRIPT_P2SH_HEX = 'a9148c108385c434a23c14f238387336ba7e31a9353487';
const LOCKSCRIPT_P2SH = Bytes.fromHex(LOCKSCRIPT_P2SH_HEX).toBytes();

// Invalid CashAddr (Bad Checksum)
const INVALID_CASH_ADDR = 'bitcoincash:pzxppqu9cs62y0q57gursuekhflrr2f4xsduyyy82l'

// Invalid Legacy Address (Bad Checksum)
const INVALID_LEGACY_ADDRESS = '1BPuKwKMywTtfBmt8aTBFk8StQtHdnqdxQ'

test("fromCashAddress instantiates from P2PKH CashAddr", t => {
  const address = Address.fromCashAddr(CASH_ADDR_P2PKH);

  t.deepEqual(address.addressContents.payload, RIPEMD160_P2PKH);
});

test("fromLegacyAddress instantiates from P2PKH Legacy Address", t => {
  const address = Address.fromLegacy(LEGACY_ADDRESS_P2PKH);

  t.deepEqual(address.addressContents.payload, RIPEMD160_P2PKH);
});

test("fromCashAddrOrLegacy instantiates from P2PKH CashAddr", t => {
  const address = Address.fromCashAddrOrLegacy(CASH_ADDR_P2PKH);

  t.deepEqual(address.addressContents.payload, RIPEMD160_P2PKH);
});

test("fromCashAddrOrLegacy instantiates from P2PKH Legacy Address", t => {
  const address = Address.fromCashAddrOrLegacy(LEGACY_ADDRESS_P2PKH);

  t.deepEqual(address.addressContents.payload, RIPEMD160_P2PKH);
});

test("fromCashAddress instantiates from P2SH CashAddr", t => {
  const address = Address.fromCashAddr(CASH_ADDR_P2SH);

  t.deepEqual(address.addressContents.payload, RIPEMD160_P2SH);
});

test("fromLegacyAddress instantiates from P2SH Legacy Address", t => {
  const address = Address.fromLegacy(LEGACY_ADDRESS_P2SH);

  t.deepEqual(address.addressContents.payload, RIPEMD160_P2SH);
});

test("fromCashAddrOrLegacy instantiates from P2SH CashAddr", t => {
  const address = Address.fromCashAddrOrLegacy(CASH_ADDR_P2SH);

  t.deepEqual(address.addressContents.payload, RIPEMD160_P2SH);
});

test("fromCashAddrOrLegacy instantiates from P2SH Legacy Address", t => {
  const address = Address.fromCashAddrOrLegacy(LEGACY_ADDRESS_P2SH);

  t.deepEqual(address.addressContents.payload, RIPEMD160_P2SH);
});

test("fromLockscriptBytes instantiates from P2SH Lockscript Bytes", t => {
  const address = Address.fromLockscriptBytes(LOCKSCRIPT_P2SH);

  t.deepEqual(address.addressContents.payload, RIPEMD160_P2SH);
});

test("fromLockscriptHex instantiates from P2SH Lockscript Hex", t => {
  const address = Address.fromLockscriptHex(LOCKSCRIPT_P2SH_HEX);

  t.deepEqual(address.addressContents.payload, RIPEMD160_P2SH);
});

test("fromCashAddr throws error if invalid CashAddr", t => {
  t.throws(() => {
    Address.fromCashAddr(INVALID_CASH_ADDR);
  });
});

test("fromLegacy throws error if invalid Base58 Address", t => {
  t.throws(() => {
    Address.fromCashAddr(INVALID_LEGACY_ADDRESS);
  });
});
