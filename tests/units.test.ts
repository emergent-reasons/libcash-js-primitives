import test from "ava";
import { Units } from "../lib/units.js";

test("toSats", t => {
  let units = new Units(100_000_000);
  t.is(units.toSats(), 100_000_000);
});

test("toBits", t => {
  let units = new Units(100_000_000);
  t.is(units.toBits(), 1_000_000);
});

test("toMBCH", t => {
  let units = new Units(100_000_000);
  t.is(units.toMBCH(), 1_000);
});

test("toBCH", t => {
  let units = new Units(100_000_000);
  t.is(units.toBCH(), 1);
});

test("fromSats", t => {
  let units = Units.fromSats(100_000_000);
  t.is(units.toSats(), 100_000_000);
});

test("fromBits", t => {
  let units = Units.fromBits(1_000_000);
  t.is(units.toSats(), 100_000_000);
});

test("fromMBCH", t => {
  let units = Units.fromMBCH(1000);
  t.is(units.toSats(), 100_000_000);
});

test("fromBCH", t => {
  let units = Units.fromBCH(1);
  t.is(units.toSats(), 100_000_000);
});

test('roundToDigits', t => {
	const testNumbers = [
		{ input: 0.1 + 0.2, expected: 0.3, digits: 1 },
		{ input: 0.1 + 0.7, expected: 0.8, digits: 1 },
		{ input: 4.6 * 100_000_000, expected: 460000000, digits: 0 },
	];

	for(const { input, digits, expected } of testNumbers)
	{
		t.is(Units.roundToDigits(input, digits), expected);
	}
})
