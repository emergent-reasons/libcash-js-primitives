/**
* Units Primitive
*/
export class Units
{
	public readonly satoshis: number;

	/**
	 * Construct a new instance of Units.
	 *
	 * This is the same as using fromSats().
	 *
	 * @param satoshis {number} The satoshi amount.
	 */
	constructor(satoshis: number) {
		// If the number is not an integer ("whole number"), throw an error.
		if(!Number.isInteger(satoshis))
		{
			throw(new Error(`Failed to instantiate Units: ${satoshis} is not an integer.`));
		}

		// Set the satoshi amount.
		this.satoshis = satoshis;
	}

	/**
	 * Instantiates from a Satoshi Amount.
	 *
	 * @param satoshis {number} The Satoshi amount.
	 *
	 * @returns {Units} New instance of Units.
	 */
	public static fromSats(satoshis: number): Units {
		return new Units(satoshis);
	}

	/**
	 * Instantiates from a Bits Amount (100 satoshis = 1 bit).
	 *
	 * @param satoshis {number} The Bits amount.
	 *
	 * @returns {Units} New instance of Units.
	 */
	public static fromBits(bits: number): Units {
		const satoshis = Units.roundToDigits(bits * 100, 0);

		return new Units(satoshis);
	}

	/**
	 * Instantiates from a mBCH Amount (100,000 satoshis = 1 mBCH).
	 *
	 * @param satoshis {number} The mBCH amount.
	 *
	 * @returns {Units} New instance of Units.
	 */
	public static fromMBCH(mBCH: number): Units {
		const satoshis = Units.roundToDigits(mBCH * 100_000, 0);

		return new Units(satoshis);
	}

	/**
	 * Instantiates from a BCH Amount (100,000,000 satoshis = 1 BCH).
	 *
	 * @param satoshis {number} The BCH amount.
	 *
	 * @returns {Units} New instance of Units.
	 */
	public static fromBCH(bch: number): Units {
		const satoshis = Units.roundToDigits(bch * 100_000_000, 0);

		return new Units(satoshis);
	}

	/**
	 * Rounds a number to the given number of digits.
	 *
	 * @param numberToRound {number} The number to round.
	 * @param digits        {number} The number of digits to round to.
	 *
	 * @returns {Number} The rounded number.
	 */
	public static roundToDigits(numberToRound: number, digits: number): number
	{
		// Set the options of the Number Format object.
		const options =
		{
			style: 'decimal',
			minimumFractionDigits: digits,
			maximumFractionDigits: digits,
			useGrouping: false,
			numberingSystem: 'latn',
		};

		// Create an instance of number format using above options.
		// NOTE: We force the locale to en-GB so that the number is formatted correctly (e.g. with a decimal, not a comma).
		const numberFormat = new Intl.NumberFormat('en-GB', options);

		// Format the number.
		const formattedAmount = numberFormat.format(numberToRound);

		// Return the formatted number.
		return Number(formattedAmount);
	}

	/**
	 * Converts the amount to Satoshis.
	 *
	 * @returns {number} The satoshi amount.
	 */
	public toSats(): number {
		return this.satoshis;
	}

	/**
	 * Converts the amount to Bits (100 satoshis = 1 bit).
	 *
	 * @returns {number} The bit amount.
	 */
	public toBits(): number {
		return Units.roundToDigits(this.satoshis / 100, 2);
	}

	/**
	 * Converts the amount to mBCH (100,000 satoshis = 1 mBCH).
	 *
	 * @returns {number} The mBCH amount.
	 */
	public toMBCH(): number {
		return Units.roundToDigits(this.satoshis / 100_000, 5);
	}

	/**
	 * Converts the amount to BCH (100,000,000 satoshis = 1 BCH).
	 *
	 * @returns {number} The BCH amount.
	 */
	public toBCH(): number {
		return Units.roundToDigits(this.satoshis / 100_000_000, 8);
	}
}
